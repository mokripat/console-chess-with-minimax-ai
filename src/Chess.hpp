#include "Game.hpp"
#include <ctime>

using namespace std;

/**
 *  Chess class serves as the whole application, implementing main menu
 *  with choice of new game with different gamemodes, capability to load
 *  up games or show instructions how to play
 * 
 *  --This and Game class is working only on Linux because of used commands system() with clear,mkdir and stat command 
 */
class Chess {
    public:
        /**default costructor*/
        Chess() {
            option = 0;
        }

        /**starts the app*/
        void start();

    private:
        /**main menu loop with switches, its being navigated through input 1-4 by choice 1-4 in menu*/
        void mainMenu();
        /**gameMode choose loop with switches, its being navigated as well as main menu, ale will start
        specific gameMode chosed by input*/
        void gamemodeMenu();
        /**menu with choices of to load previously saved games and capability to delete old saves
        to make space for new ones*/
        void gameloadMenu();
        /**reads input and returns 1-4 option, also handles EOF*/
        int readInput() const;
        /**reads input and returns 1-n option, also handles EOF*/
        int readInput(int optionCount) const;
        /**holds all the couts*/
        void printOptions(int stage);
        /**prints out ASCII-art Chess logo*/
        void printLogo() const;
        /**print howToPlay instructions and tips*/
        void showInstuctions();
        /**exits the app*/
        void Exit();
        /**gamemode variable*/
        int option;
        /**variable to track number of saves so it wont exceed 9 saves*/
        int savesCount; 
};