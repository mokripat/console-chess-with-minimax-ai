#include "Chess.hpp"

//constant when coverting int number to ascii char code
const int INT_ASCII_OFFSET = 48;

/** Starts the application*/
void Chess::start() {
    mainMenu();
}

/** Main menu made up with simple switch*/
void Chess::mainMenu() {
    system("clear");
    printOptions(0);

    /**move to next stage by given output*/
    switch (readInput()) {
        // proceeding to new game
        case 1:
            gamemodeMenu();
            break;
        // proceeding to load game
        case 2:
            gameloadMenu();
            break;
        // showing Chess instructions and returning to main menu
        case 3:
            showInstuctions();
            mainMenu();
            break;
        // exiting the game
        case 4:
            Chess::Exit();
            return;

        default:
            break;
    }

    return;
}

/** Gamemode menu before starting new game*/
void Chess::gamemodeMenu() {
    system("clear");
    printOptions(1);

    Game newOne;

    switch (readInput()) {
        // PVP
        case 1:
            newOne.newGame(false);
            break;
        // random moves
        case 2:
            newOne.newGame(true,0);
            break;
        // easy AI
        case 3:
            newOne.newGame(true,1);
            break;
        // medium AI
        case 4:
            newOne.newGame(true,2);
            break;

        default:
            mainMenu();
            break;
        }
}

/** Choosing which loadfile user wants to load up*/
void Chess::gameloadMenu() {
    system("clear");
    printOptions(2);
    
    if(savesCount) {
        Game loaded;
        string loadDest = "saves/save";
        // getting choice from input
        int choice = readInput(savesCount);
        // if choice != 0 load selected save file to load up
        if(choice) {
            loadDest.push_back(choice + INT_ASCII_OFFSET);
            std::cout << loadDest << std::endl;
            loaded.loadGame(loadDest);
        // choice = 0 -> delete all save files
        } else {
            system("rm -rf saves/");
            system("mkdir saves");
            mainMenu();
        } 
        
    } 
}

/** Reads input and returns 1-4 option, also handles EOF*/
int Chess::readInput() const {
    string read;
    while(1) {
        getline(cin,read);
        if(read[0] >= '1' && read[0] <= '4')
            return int(read[0] - INT_ASCII_OFFSET);
        //if wrong input was read
        std::cout << "You trollin'\n";
        //eof
        if(std::cin.eof()) {
            return 0;
        }
    }
}

/** Reads input and returns 1-n option, also handles EOF*/
int Chess::readInput(int optionCount) const {
    string read;
    while(1) {
        getline(cin,read);
        if(read[0] >= '0' && read[0] < (optionCount + INT_ASCII_OFFSET))
            return int(read[0] - INT_ASCII_OFFSET);
        //if wrong input was read
        std::cout << "You trollin'\n";
        //eof
        if(std::cin.eof()) {
            return 0;
        }
    }
}

/**instructions print + wait till player presses any button*/
void Chess::showInstuctions() {
    system("clear");
    printOptions(3);
    char temp = 'x';
    while (temp != '\n')
        cin.get(temp);
    return;
}

/**printing method, if stage = 2, it reads through saves folder, display files and saves how much files are in saves*/
void Chess::printOptions(int stage) {
    std::string saveFileName;
    int i = 1;
    switch(stage) {
        case 0:
            printLogo();

            std::cout << "Main menu: \n\t1 -> NewGame\n\t2 -> LoadGame\n\t3 -> Instructions\n\t4 -> Exit\n";

            break;
        case 1:
            printLogo();

            std::cout << "Select gamemode:\n\t1 - PvP\n\t2 - Experiment vs random moves\n\t3 - NoBrain opponent\n\t4 - SomeBrain opponent\n";
            break;
        
        case 2:
            printLogo();
            
            system("mkdir -p saves");
            std::cout << "Choose a save file:\n\t0. Delete all current save files\n";
            while (true) {
                saveFileName = "saves/save";
                saveFileName.push_back((i+INT_ASCII_OFFSET));
        
                struct stat buffer;
                if(stat(saveFileName.c_str(), &buffer) != 0) {
                    break;
                } else {
                    struct tm * timeinfo = localtime(&buffer.st_ctime);
                    std::cout << "\t" << i << ". save from " << asctime(timeinfo);
                    i++;       
                }
            }
            if(!i)
                std::cout << "No save files found..\n";

            savesCount = i;    
            break;

        case 3:
            std::cout <<    "------------------------INSTRUCTIONS------------------------\n\n"
                            "How to play:\n"
                            "Control of chess piece is read from input. Simply type\n"
                            "position of your figure and destination square you want\n"
                            "your figure move to. Correct format of input is f.e. \"b0 c2\"\n\n"
                            "If you type wrong command, error phrase will pop-up and you\n"
                            "will get an opportunity to re-type. Game will not let you\n"
                            "move piece if your King would get in danger or when you\n"
                            "don't get him out of check when you are able to.\n\n"
                            "This version of Chess supports all basic rules of chess\n"
                            "with one small exception and that's pawns will be always\n"
                            "promoted to queen.\n\n"
                            "If you would like to save the game use shortcut CTRL-D and\n"
                            "current match will be automatically saved including chosen\n"
                            "gamemode (and difficulty in game versus AI).\n"
                            "\nPress ENTER to go back...\n\n";
            break;
        default:
            break;
    }

    return;

}

/** Prints Chess logo as ASCII art*/
void Chess::printLogo() const {
    std::cout << 
            "    _____  _\n"                     
            "   / ____|| |\n"                    
            "  | |     | |__    ___  ___  ___\n" 
            "  | |     | '_ \\  / _ \\/ __|/ __|\n"
            "  | |____ | | | ||  __/\\__ \\__  \\\n"
            "   \\_____||_| |_| \\___||___/|___/\n\n";
    return;
}

/** Very simplistic exit*/
void Chess::Exit() {
    std::cout << "Bye byee\n";
}