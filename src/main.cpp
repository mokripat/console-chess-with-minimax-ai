#include <iostream>
#include <csignal>
#include "Chess.hpp"

using namespace std;

void signalHandler (int) {
   exit(0);
}

int main() {
    signal(SIGTERM, signalHandler);
    signal(SIGINT, signalHandler);
    Chess chess;
    chess.start();

    return 0;   
}