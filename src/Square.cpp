#include "Square.hpp"  

/** Defaultly sets square to non-occupied state*/
Square::Square() {
    occupied = false;
    currFig = nullptr;
}

/** Constructor of square right away with figure*/
Square::Square(Figure * nFigure) {
    occupied = true;
    currFig = nFigure;
}

/** If square is occupied return type of that piece*/
char Square::getType() const {
    if(isOccupied())
        return currFig->getType();
    else 
        return '0';
}

/** If square is occupied return color of that piece, default is white to avoid segmentFault */
bool Square::getColor() const {
    if(isOccupied())
        return currFig->getColor();
    else 
        return true;
}

/** If square is occupied sets to non-occupied state */
bool Square::removePiece() {
    if(!occupied) {
        std::cerr << "Square is not occupied" << std::endl;
        return false;
    }
    currFig = nullptr;
    occupied = false;
    return true;
}

/** If square is not occupied sets to occupied state*/
bool Square::setPiece( Figure * newFigure) {
    if(occupied) {
        std::cerr << "this square is already occupied" << std::endl;
        return false;
    }
    currFig = newFigure;
    occupied = true;
    return true;
}