#pragma once

#include <iostream>
#include <memory>
#include <string>
#include "Figures.hpp"

/**
 * Square class represents a single square in chessboard
 * holding information if its occupied respectively by which piece with all its information
 */

class Square {
	public:
		/**constructor*/
		Square();
		/**constructor with particular figure*/
		Square(Figure * nFigure);

		/**getter of figure type that occupies the square*/
		char getType() const;
		/**getter of figure color that occupies the square | true - white, false - black*/
		bool getColor() const;
		/**getter of figure that occupies the square*/
		Figure * getPiece() const { return currFig;}

		/**polymorhip method which returns if move of a figure that occupies this square on empty chessboard is valid no matter what piece it is*/
		bool isMoveValid( int & x1, int & y1, int & x2, int & y2) const { return currFig->moveValidator( x1, y1, x2, y2);};
		/**return if this square is currently occupied*/
		bool isOccupied() const { return occupied;};
		/**removes piece from square | sets the square to non-occupied state*/
		bool removePiece();
		/**sets piece on square | sets the square to ocuppied state*/
		bool setPiece(Figure * newFigure);
	
	private:
		bool occupied;
		Figure * currFig;
		int x,y;
};