#pragma once

#include "Chessboard.hpp"
#include "ChessAI.hpp"
#include <fstream>
#include <string>
#include <sstream>
#include <sys/stat.h>

/**
 * Game class serves as whole control of a single match
 * it provides intilization of the match, saving match
 * and loading the match with previous settings
 * 
 * --This and Chess class is working only on Linux because of used commands system() with clear,mkdir and stat command 
 */

class Game {
    public:
        /**initialize and starts new match witch correct gamemode and difficulty*/
        void newGame(bool opponent, int difficulty = 0);
        /**load up game from saves directory chosen by user with correct gamemode and difficulty*/
        bool loadGame(std::string loadFileName);
        /**saves the game into saves directory remembering all important info from the match
        and encodes it so it couldn't be sabotaged*/
        void saveGame() const;

        /**useful variable while loading a file serving as flag for Chess class to give
        user opportunity to chose another file*/
        bool corruptFlag = false;
    private:
        /**starts up game for two players*/
        void playVersus();
        /**starts up game versus a computer that play random yet correct plays*/
        void playRandom();
        /**starts up game versus a computer with chosen difficulty, this version is created for begginers
        as me to learn chess so it has easy and medium difficulty*/
        void playAI(int difficulty);

        /**generates code for given coordinates with pieces for safe save file having ability being immune for
        non-expert sabotage*/
        std::string generateSaveCode(std::vector<std::pair<coordinates,Figure*>> figureList) const;

        /**allocates figure to be used in freshly loaded match, if wrong combination is given it throws exception*/
        Figure * createPiece(char figure, bool color) const;
        /**handles uninitialized values and set them up for smooth match*/
        void setUpChessboard(Chessboard & loaded);

        /**variable for current chessboard*/
        Chessboard currGame;
        /**chosen gamemode*/
        int gamemode;
        /**track and store information if game is in progress*/
        bool gameRunning;
};