#pragma once

#include "coordinates.hpp"
#include "Chessboard.hpp"
#include <cstdlib>
#include <cmath>
#include <ctime>

/**
 * ChessAI = Computer AI for Chess which produces random moves or produce moves in 2 difficulties (they depend on the depth of minimax).
 * This version of chess is created and designed for newbies such as me to learn so it provides random, easy and medium difficulty
 * 
 * by 1/5/2020 AI is working correctly but is very slow
 * 
 * by 21/5/2020 random AI calculates next move instantly
 *              easy AI takes around 0.5-1.5 second to calculate next move
 *              medium AI takes around 1-5 seconds to calculate next move (moves sometimes look dumb and often are but newbies can easily get fooled by them)
 *              AI was speed up by implementing alpha-beta pruning
 */

class ChessAI {
    public:
        /**default constructor*/
        ChessAI() {
            difficulty = 0;
        }

        /**constructor with difficulty, 0 - random, 1 - easy, 2 - medium .. higher diff would make minimax go for a little while :]*/
        ChessAI(int diff) {
            diff = diff > 2 ? 2 : diff;
            difficulty = diff;
        }

        /**produces and plays random moves, based on generating all possible moves and then selecting random figure and move*/
        void playRandom(Chessboard & chessboard) const;
        /**plays best move generated from findBestMove() which is based on minimax algorithm*/
        void playMinimax(Chessboard & chessboard) const;
        /**generates best possible move found by minimax algorithm*/
        std::pair<coordinates,coordinates> findBestMove (Chessboard chessboard, int depth) const;
        /**minimax for chess, it stops on specified depth*/
        int minimax(Chessboard chessboard, int depth, int alpha, int beta, bool maximizingPlayer) const;
        

    private:
        /**holds information what difficulty was chosen by player*/
        int difficulty;
};