#pragma once

#include <iostream>
#include <cstdlib>
#include <string>

/**
 *	Figures file holds base class and its derived classes for each figure.
 *  Figures hold basic variables - type, color and value - to be easy to work with
 *  Polymorphism is used in moveValidator, which functionality is explained below, its then called in Chessboard class in function tryMove() <<<<--------
 * 	its designed in way I dont need to know with which piece I am working, I can get that info easy or call the moveValidator and don't mind
 *  I like to keep them together and have each polymorphic method moveValidator next to other
 */

class Figure {
	protected:
		char m_type;
		/**white = true, black = false .. I used bool so I can compare it with bool playerTurn later (which matches)*/
		bool m_color;
		int m_value;

	public:
		/**checks if this move is possible for specific figure (out of bounds, correct move) not counting collisions, 
		they are handled later in tryMove() in Chessboard class*/
		virtual bool moveValidator( int x1, int y1, int x2, int y2) = 0; //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Polymorphism
		/**getters*/
		int getValue() const;
		char getType() const;
		bool getColor() const;
		virtual ~Figure(){};
};
/** Child of Figure that represents pawn figure */
class Pawn : public Figure {
	public:
		virtual bool moveValidator( int x1, int y1, int x2, int y2) override;
		Pawn(char color);
};
/** Child of Figure that represents rook figure */
class Rook : public Figure {
	public:
		virtual bool moveValidator( int x1, int y1, int x2, int y2) override;
		Rook(char color);
};
/** Child of Figure that represents bishop figure */
class Bishop : public Figure {
	public:
		virtual bool moveValidator( int x1, int y1, int x2, int y2) override;
		Bishop(char color);
};
/** Child of Figure that represents king figure */
class King : public Figure {
	public:
		virtual bool moveValidator( int x1, int y1, int x2, int y2) override;
		King(char color);
};
/** Child of Figure that represents knight figure */
class Knight : public Figure {
	public:
		virtual bool moveValidator( int x1, int y1, int x2, int y2) override;
		Knight(char color);
};
/** Child of Figure that represents queen figure */
class Queen : public Figure {
	public:
		virtual bool moveValidator( int x1, int y1, int x2, int y2) override;
		Queen(char color);
};
