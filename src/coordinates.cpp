#include "coordinates.hpp"

/** Operator < so coordinates can be used in map*/
bool coordinates::operator < (const coordinates & x) const {
    if(m_x != x.m_x)
        return m_x < x.m_x;
    return m_y < x.m_y;
}
/** Operator = for utility*/
void coordinates::operator = (const coordinates & x) {
    m_x = x.m_x;
    m_y = x.m_y;
}