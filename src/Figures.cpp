#include "Figures.hpp"

const int BOARD_MAX = 7;

/** Figure color getter*/
bool Figure::getColor() const {
    return m_color;
}

/** Figure type getter*/
char Figure::getType() const {
    return m_type;
}

/** Figure value getter, white values are negative, black positive so black can be maximizing player in Minimax algorithm*/
int Figure::getValue() const {
    if(!m_color)
        return m_value;
    return (m_value * -1);
}

bool Pawn::moveValidator( int x1, int y1, int x2, int y2) {
    //pawn cant go back
    if((m_color == true &&  y2 < y1) || (m_color == false && y2 > y1))
        return false;
    //if pawn moved, he can move only foward 1 square
    else if((m_color == true && y1 != 1 && (y2 - y1) > 1) || (m_color == false && y1 != 6 && (y1 - y2 > 1)))
        return false;
    //pawn cant move to sides
    else if(y1 == y2)
        return false;
    //pawn can move diagonaly to remove enemy piece BUT its not a knight.. just simple pawn
    else if(abs(x2 - x1) > 1)
        return false;
    //check for pawn's first move
    else if(((m_color == true && y1 == 1) && ((y2 - y1) > 2)) || ((m_color == false && y1 == 6) && ((y1 - y2) > 2)))
        return false;
    //first move can be as expected only foward  
    else if(abs(y2 - y1) == 2 && x1 != x2)
        return false;
    else return true;
}

/** Pawn costructor*/
Pawn::Pawn(char color){
    m_color = color;
    m_type = 'p';
    m_value = 10;
}


bool Rook::moveValidator( int x1, int y1, int x2, int y2) {
    //to sides / up/down movement
    if((y1 == y2) || (x1 == x2))
        return true;

    return false;
}

/** Rook constructor*/
Rook::Rook(char color) {
    m_color = color;
    m_type = 'R';
    m_value = 50;
}

bool Bishop::moveValidator( int x1, int y1, int x2, int y2) {
    //diagonal moves
    for ( int i = 1; i <= BOARD_MAX; ++i) {
        //down-left
        if((x2 + i) == x1 && (y2 + i) == y1)
            return true;
        //up-right
        if((x2 - i) == x1 && (y2 - i) == y1)
            return true;
        //down-right
        if((x2 - i) == x1 && (y2 + i) == y1)
            return true;
        //up-left
        if((x2 + i) == x1 && (y2 - i) == y1)
            return true;
    }

    return false;
}

/** Bishop constructor*/
Bishop::Bishop(char color) {
    m_color = color;
    m_type = 'B';
    m_value = 30;
}


bool King::moveValidator ( int x1, int y1, int x2, int y2) {
    if(x2 - 1 == x1 && y1 == y2)
        return true;
    else if(x2 + 1 == x1 && y1 == y2)
        return true;
    else if(y2 + 1 == y1 && x1 == x2)
        return true;
    else if(y2 - 1 == y1 && x1 == x2)
        return true;
    else if(y2 + 1 == y1 && x1 == x2 + 1)
        return true;
    else if(y2 - 1 == y1 && x1 == x2 - 1)
        return true;
    else if(y2 + 1 == y1 && x1 == x2 - 1)
        return true;
    else if(y2 - 1 == y1 && x1 == x2 + 1)
        return true;
    else return false;
}

/** King constructor*/
King::King(char color) {
    m_color = color;
    m_type = 'K';
    m_value = 10000;
}

bool Knight::moveValidator ( int x1, int y1, int x2, int y2) {
    //first and prob only use of abs() in validators, cause it was the only
    //piece whose movement I needed to draw and figure out  
    int a = abs(x1 - x2);
    int b = abs(y1 - y2);

    if ((a == 1 && b == 2) || (a == 2 && b == 1))
        return true;
    else return false;
}

/** Knight constructor*/
Knight::Knight(char color) {
    m_color = color;
    m_type = 'N';
    m_value = 30;
}

bool Queen::moveValidator ( int x1, int y1, int x2, int y2) {
    //to sides / up/down movement 
    if((y1 == y2) || (x1 == x2))
        return true;
    //diagonal moves... could be simplified with abs()
    for ( int i = 1; i <= BOARD_MAX; ++i) {
        //down-left
        if((x2 + i) == x1 && (y2 + i) == y1)
            return true;
        //up-right
        if((x2 - i) == x1 && (y2 - i) == y1)
            return true;
        //down-right
        if((x2 - i) == x1 && (y2 + i) == y1)
            return true;
        //up-left
        if((x2 + i) == x1 && (y2 - i) == y1)
            return true;
    }

    return false;
}

/** Queen constructor*/
Queen::Queen(char color) {
    m_color = color;
    m_type = 'Q';
    m_value = 90;
}