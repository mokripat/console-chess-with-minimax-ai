#pragma once

/**
 * coordinates class represents single x,y position to easier implementation and readablity of the code
 * added lately for easier implementation of validMovesGen, minimax and needs of returning x,y at the same time
 */
struct coordinates {
    int m_x,m_y;

    /**default constructor, -1 so if by any chance coor were inicialized this way, it wont go through tryMove()*/
    coordinates() {
        m_x = -1;
        m_y = -1;
    }

    /**constructor*/
    coordinates(int x, int y) : m_x(x), m_y(y){};

    /**operator < so coordinates can be used in map*/
    bool operator < (const coordinates & x) const;

    /**operator = for utility*/
    void operator = (const coordinates & x);
};