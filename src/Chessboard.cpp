#include "Chessboard.hpp"

//board size constants
const int BOARD_X_MIN = 0;
const int BOARD_Y_MIN = 0;
const int BOARD_X_MAX = 7;
const int BOARD_Y_MAX = 7;

//constants when coverting int number to ascii char code or int to char to ascii char code
const int INT_ASCII_OFFSET = 48;
const int CHAR_ASCII_OFFSET = 65;

/** Sets cout and cerr stream to failbit so its muted and all output is discarted*/
void silenceOutput() {
    std::cout.setstate(std::ios_base::failbit);
    std::cerr.setstate(std::ios_base::failbit);
}

/** Unsets cout and cerr stream from failbit to goodbit so output is re-enabled*/
void unsilenceOutput() {
    if(std::cout.fail())
        std::cout.clear();
    if(std::cerr.fail())
        std::cerr.clear();
}

/** Sets new board and initialize king-managment variables*/
void Chessboard::setBoard() {
    gameRunning = true;
    playerTurn = true;

    wKingPos[0] = 4;
    wKingPos[1] = 0;

    bKingPos[0] = 4;
    bKingPos[1] = 7;

    wKingInCheck = false;
    bKingInCheck = false;

    Pawn * wPawn0 = new Pawn(true);                                       //
    square[0][1].setPiece(wPawn0);                                        //
                                                                          //   
    Pawn * wPawn1 = new Pawn(true);                                       //
    square[1][1].setPiece(wPawn1);                                        //
                                                                          //
    Pawn * wPawn2 = new Pawn(true);                                       //
    square[2][1].setPiece(wPawn2);                                        //
                                                                          //
    Pawn * wPawn3 = new Pawn(true);                                       //
    square[3][1].setPiece(wPawn3);                                        //
                                                                          //
    Pawn * wPawn4 = new Pawn(true);                                       //
    square[4][1].setPiece(wPawn4);                                        //
                                                                          //
    Pawn * wPawn5 = new Pawn(true);                                       //        +
    square[5][1].setPiece(wPawn5);                                        //        |
                                                                          //
    Pawn * wPawn6 = new Pawn(true);                                       //        W
    square[6][1].setPiece(wPawn6);                                        //        H
                                                                          //        I
    Pawn * wPawn7 = new Pawn(true);                                       //        T
    square[7][1].setPiece(wPawn7);                                        //        E
                                                                          //
    Rook * wRook0 = new Rook(true);                                       //        |
    square[0][0].setPiece(wRook0);                                        //        +
                                                                          //
    Knight * wKnight0 = new Knight(true);                                 //
    square[1][0].setPiece(wKnight0);                                      //
                                                                          //
    Bishop * wBishop0 = new Bishop(true);                                 //
    square[2][0].setPiece(wBishop0);                                      //
                                                                          //
    Queen * wQueen = new Queen(true);                                     //
    square[3][0].setPiece(wQueen);                                        //
                                                                          //
    King * wKing = new King(true);                                        //
    square[4][0].setPiece(wKing);                                         //
                                                                          //
    Bishop * wBishop1 = new Bishop(true);                                 //
    square[5][0].setPiece(wBishop1);                                      //
                                                                          //
    Knight * wKnight1 = new Knight(true);                                 //
    square[6][0].setPiece(wKnight1);                                      //
                                                                          //
    Rook * wRook1 = new Rook(true);                                       //
    square[7][0].setPiece(wRook1);                                        //
                                                                     //*******//
    Pawn * bPawn0 = new Pawn(false);                                      //
    square[0][6].setPiece(bPawn0);                                        //
                                                                          //   
    Pawn * bPawn1 = new Pawn(false);                                      //
    square[1][6].setPiece(bPawn1);                                        //
                                                                          //
    Pawn * bPawn2 = new Pawn(false);                                      //
    square[2][6].setPiece(bPawn2);                                        //
                                                                          //
    Pawn * bPawn3 = new Pawn(false);                                      //
    square[3][6].setPiece(bPawn3);                                        //
                                                                          //
    Pawn * bPawn4 = new Pawn(false);                                      //
    square[4][6].setPiece(bPawn4);                                        //
                                                                          //
    Pawn * bPawn5 = new Pawn(false);                                      //        +
    square[5][6].setPiece(bPawn5);                                        //        |
                                                                          //
    Pawn * bPawn6 = new Pawn(false);                                      //        B
    square[6][6].setPiece(bPawn6);                                        //        L
                                                                          //        A
    Pawn * bPawn7 = new Pawn(false);                                      //        C
    square[7][6].setPiece(bPawn7);                                        //        K
                                                                          //
    Rook * bRook0 = new Rook(false);                                      //        |
    square[0][7].setPiece(bRook0);                                        //        +
                                                                          //
    Knight * bKnight0 = new Knight(false);                                //
    square[1][7].setPiece(bKnight0);                                      //
                                                                          //
    Bishop * bBishop0 = new Bishop(false);                                //
    square[2][7].setPiece(bBishop0);                                      //
                                                                          //                                                                                                                        
    Queen * bQueen = new Queen(false);                                    //
    square[3][7].setPiece(bQueen);                                        //
                                                                          //
    King * bKing = new King(false);                                       //
    square[4][7].setPiece(bKing);                                         //
                                                                          //
    Bishop * bBishop1 = new Bishop(false);                                //
    square[5][7].setPiece(bBishop1);                                      //
                                                                          //
    Knight * bKnight1 = new Knight(false);                                //
    square[6][7].setPiece(bKnight1);                                      //
                                                                          //
    Rook * bRook1 = new Rook(false);                                      //
    square[7][7].setPiece(bRook1);                                        //
}

/** Deletes pieces from board and from vector that holds removed pieces (frees all allocated figures)*/
void Chessboard::clearBoard() {
    //from board
    for (int i = BOARD_X_MIN; i <= BOARD_X_MAX; ++i) {
        for (int j = BOARD_Y_MIN; j <= BOARD_Y_MAX; ++j) {
            if(square[i][j].isOccupied()) {
                delete square[i][j].getPiece();
            }
        }
    }
    //from remodedP vector
    for (auto it = removedPieces.begin(); it != removedPieces.end(); ++it ) {
        delete *it;
    }
}

/** Goes through Chessboard, if piece is found push_back it into vector with position and eventually return the vector */
std::vector<std::pair<coordinates,Figure*>> Chessboard::getAllFigures() const {
    std::vector<std::pair<coordinates,Figure*>> allpieces;
    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j < 8; ++j) {
            if(square[i][j].isOccupied()) {
                allpieces.push_back(std::make_pair(coordinates(i,j),square[i][j].getPiece()));
            }
        }
    }
    return allpieces;
}

/** Reads and update from board both kings position, if they are in check and if castling is available*/
void Chessboard::updateKingsPos() {
    //kings positions
    for(int i = BOARD_X_MIN; i <= BOARD_X_MAX; ++i) {
        for(int j = BOARD_Y_MIN; j <= BOARD_Y_MAX; ++j) {
            if(square[i][j].isOccupied()) {
                if(square[i][j].getType() == 'K') {
                    if(square[i][j].getColor()) {
                        wKingPos[0] = i;
                        wKingPos[1] = j;
                    } else {
                        bKingPos[0] = i;
                        bKingPos[1] = j;
                    }
                }
            }
        }
    }
    //checks
    wKingInCheck = isKingInCheck(true);
    bKingInCheck = isKingInCheck(false);
    //castling
    //white king
    if(wKingPos[0] == 4 && wKingPos[1] == 0) {
        if(square[0][0].isOccupied())
            whiteCastleAv[0] = square[0][0].getType() == 'R'? true:false;
        else
            whiteCastleAv[0] = false;
        
        if(square[7][0].isOccupied())
            whiteCastleAv[1] = square[7][0].getType() == 'R'? true:false;
        else
            whiteCastleAv[1] = false;
    } else {
        whiteCastleAv[0] = false;
        whiteCastleAv[1] = false;
    }
    //black king
    if(bKingPos[0] == 4 && bKingPos[1] == 7) {
        if(square[0][7].isOccupied())
            blackCastleAv[0] = square[0][7].getType() == 'R'? true:false;
        else
            blackCastleAv[0] = false;
        
        if(square[7][7].isOccupied())
            blackCastleAv[1] = square[7][7].getType() == 'R'? true:false;
        else
            blackCastleAv[1] = false;
    } else {
        blackCastleAv[0] = false;
        blackCastleAv[1] = false;
    }
}

/** Reads position and figure and places it on Chessboard*/
void Chessboard::setFromLoadFile(std::vector<std::pair<coordinates,Figure*>> loadFile) {
    for(auto it = loadFile.begin(); it != loadFile.end(); ++it) {
        square[it->first.m_x][it->first.m_y].setPiece(it->second);
    }
    return;
}

//POLYMORPHISM METHOD CALLED HERE .. sign <--------------
/** TryMove method recieves figure position and destination position and tests if that move is possible in full means (collisions, special rules, move of bounds etc.) */
bool Chessboard::tryMove( int x1, int y1, int x2, int y2, bool outputEnabled) const { 
    //movement out of bounds
    if( x1 < BOARD_X_MIN || x1 > BOARD_X_MAX || y1 < BOARD_Y_MIN || y1 > BOARD_Y_MAX || x2 < BOARD_X_MIN || x2 > BOARD_X_MAX || y2 < BOARD_Y_MIN || y2 > BOARD_Y_MAX)
        return false;
    
    //if output is set to be quiet, quiet it
    if(!outputEnabled) {
        silenceOutput();
    }
    //struct that unmutes output when wents out of scope
    unsilencingAfterReturn uar;

    //if there is no piece the is nothing to move
    if(!square[x1][y1].isOccupied()) {
        std::cerr << "There is no figure on " << x1 << y1 << std::endl; 
        return false;
    } 

    //target square is occupied with piece of the same color
    if(square[x2][y2].isOccupied())
        if(square[x1][y1].getColor() == square[x2][y2].getColor()){
            std::cerr << "target square is the same color!" << std::endl;    
            return false;
        }
    
    //pawn
    if( square[x1][y1].getType() == 'p') {
        if(!square[x1][y1].isMoveValid(x1,y1,x2,y2)) //<--------------
            return false;
        
        //pawn collider
        if(x1 == x2 && square[x2][y2].isOccupied()) {
            std::cerr << "pawn cant do that move, there is some piece in the way" << std::endl;
            return false;
        } else if (y1 == 1 && y2 == 3) {
            if(square[x2][2].isOccupied()) {
                std::cerr << "pawn cant do that move, there is some piece in the way" << std::endl;
                return false;
            }
        } else if (y1 == 6 && y2 == 4) {
            if(square[x2][5].isOccupied()) {
                std::cerr << "pawn cant do that move, there is some piece in the way" << std::endl;
                return false;
            }
        }

        //en passant
        if((((y1 == 4 && x1 != x2) && square[x2][y1].isOccupied())) && (x2 == lastMove[2] && y1 == lastMove[3])) {
            if(square[x2][y1].getType() == 'p' && square[x2][y1].getColor() != square[x1][y1].getColor()) {
               return true;
            } 

        } else if ((((y1 == 3 && x1 != x2) && square[x2][y1].isOccupied())) &&(x2 == lastMove[2] && y1 == lastMove[3])) {
            if(square[x2][y1].getType() == 'p' && square[x2][y1].getColor() != square[x1][y1].getColor()) {
               return true; 
            }
        }

        if(x1 != x2 && !square[x2][y2].isOccupied()) {
            std::cerr << "pawn cant do that move" << std::endl;
            return false;
        }   
    }

    //rook
    else if( square[x1][y1].getType() == 'R') {
        if(!square[x1][y1].isMoveValid(x1,y1,x2,y2)) //<--------------
            return false;

        //rook collider
        if (x1 == x2) {
            int yIncrement = (y2 - y1) / (abs(y2 - y1));
            for (int i = y1 + yIncrement; i != y2; i += yIncrement) {
				if (square[x2][i].isOccupied()){
					std::cerr << "rook cant do that move, there is some piece in the way" << std::endl;
                    return false;
                }
            }
        } else {
            int xIncrement = (x2 - x1) / (abs(x2 - x1));
            for (int i = x1 + xIncrement; i != x2; i += xIncrement) {
				if (square[i][y2].isOccupied()){
                    std::cerr << "rook cant do that move, there is some piece in the way" << std::endl;
					return false;
                }
            }
        }
    }

    //bishop
    else if( square[x1][y1].getType() == 'B') {
        if(!square[x1][y1].isMoveValid(x1,y1,x2,y2)) //<--------------
            return false;

        //bishop collider        
        int xIncrement = (x2 - x1) / (abs(x2 - x1));
		int yIncrement = (y2 - y1) / (abs(y2 - y1));

		for (int i = 1; i < abs(x1 - x2); i++) {
			if (square[x1 + xIncrement * i][y1 + yIncrement * i].isOccupied()) {
				std::cerr << "bishop cant do that move, there is some piece in the way" << std::endl;
                return false;
            }
		}
    }

    //knight
    else if( square[x1][y1].getType() == 'N') {
        if(!square[x1][y1].isMoveValid(x1,y1,x2,y2)) //<--------------
            return false;
    }

    //king
    else if( square[x1][y1].getType() == 'K') {
        //castling
        //whiteKing
        if((x1 == 4 && y1 == 0)) {
            //left castling
            if(x2 == 2 && y2 == 0) {
                if(isCastlingPossile(1))
                    return true;
            //right castling
            } else if(x2 == 6 && y2 == 0) {
                if(isCastlingPossile(2))
                    return true;
            }
        //blackKing
        } else if((x1 == 4 && y2 == 7)) {
            //left castling
            if(x2 == 2 && y2 == 7) {
                if(isCastlingPossile(3))
                    return true;
            //right castling
            } else if(x2 == 6 && y2 == 7) {
                if(isCastlingPossile(4))
                    return true;
            }
        }
        
        
        if(!square[x1][y1].isMoveValid(x1,y1,x2,y2)) //<--------------
            return false; 
    }

    //queen
    else if( square[x1][y1].getType() == 'Q') {
        if(!square[x1][y1].isMoveValid(x1,y1,x2,y2)) //<--------------
            return false;
        
        //queen collider 
        //  1) move like rook 
        //  2) move like bishop
        if(x1 == x2 || y1 == y2) { // 1)
            if (x1 == x2) {
                int yIncrement = (y2 - y1) / (abs(y2 - y1));
                for (int i = y1 + yIncrement; i != y2; i += yIncrement) {
                    if (square[x2][i].isOccupied()){
                        std::cerr << "queen cant do that move, there is some piece in the way" << std::endl;
                        return false;
                    }
                }
            } else {
                int xIncrement = (x2 - x1) / (abs(x2 - x1));
                for (int i = x1 + xIncrement; i != x2; i += xIncrement) {
                    if (square[i][y2].isOccupied()){
                        std::cerr << "queen cant do that move, there is some piece in the way" << std::endl;
                        return false;
                    }
                }
            }
        } else { // 2)
            int xIncrement = (x2 - x1) / (abs(x2 - x1));
		    int yIncrement = (y2 - y1) / (abs(y2 - y1));

		    for (int i = 1; i < abs(x1 - x2); i++) {
                if (square[x1 + xIncrement * i][y1 + yIncrement * i].isOccupied()) {
                    std::cerr << "queen cant do that move, there is some piece in the way" << std::endl;
                    return false;
                }
		    }
        }
    }

   return true;  
}

/** Input managment method, reads piece coords and destination coords
input is not case sensitive*/
int * Chessboard::readMove() const {
    bool invalid = true;
    char * input = new char[4];
    std::string validInput = "";

    //read loop, if the input is correct it will break
    while (invalid) {
        if(std::cin.eof())
            break;
        //formatting
        std::cout << "+------------------+\n";
        if(playerTurn)
            std::cout << "White move >> ";
        else 
            std::cout << "Black move >> ";
        
        //read
        std::string read;
        getline(std::cin, read);

        int charRead = 0;
        for(std::size_t i = 0; i < read.size(); ++i) {
            if(read[i] == ' ')
                continue;
            input[charRead] = read[i];
            charRead++;
            if(charRead == 4)
                break;
        }
        //wrong input handling
        if(charRead != 4) {
            std::cout << "# Wrong input" << std::endl;
            continue;
        }

        if(!((input[0] >= 'A' && input[0] <= 'H' ) || (input[0] >= 'a' && input[0] <= 'h' ))){
            std::cout << "# Wrong input" << std::endl;
            continue;
        }
        if(!(input[1] >= '0' && input[1] <= '7')) {
            std::cout << "# Wrong input" << std::endl;
            continue;
        }
        if(!((input[2] >= 'A' && input[2] <= 'H' ) || (input[2] >= 'a' && input[2] <= 'h' ))){
            std::cout << "# Wrong input" << std::endl;
            continue;
        }
        if(!(input[3] >= '0' && input[3] <= '7')){
            std::cout << "# Wrong input" << std::endl;
            continue;
        }
        //making numbers from chars from iput
        for (int i = 0; i < 4; ++i ) {
            if(input[i] >= 'A' && input[i] <= 'H' ) {
                input[i] -= 17;
            } else if (input[i] >= 'a' && input[i] <= 'h' ) {
                input[i] -= 49;
            }

            validInput.push_back(input[i]);
        }
        //piece coords are empty
        if(!square[(int)(validInput[0] - INT_ASCII_OFFSET)][(int)(validInput[1] - INT_ASCII_OFFSET)].isOccupied()) {
            std::cerr << "# Wrong input" << std::endl;
            validInput = "";
            continue;
        }
        //trying to move with opponent piece
        if(square[(int)(validInput[0] - INT_ASCII_OFFSET)][(int)(validInput[1] - INT_ASCII_OFFSET)].getColor() != playerTurn ) {
            std::cerr << "You cannot move your opponent's piece!" << std::endl;
            validInput = "";
            continue;
        }
        //trying to get off king
        if(square[(int)(validInput[2] - INT_ASCII_OFFSET)][(int)(validInput[3] - INT_ASCII_OFFSET)].getType() == 'K' ) {
            std::cerr << "You cannot touch the kings! Obey the rules peasant.." << std::endl;
            validInput = "";
            continue;
        }

        invalid = false;
    }
    //CTRL-D (EOF)
    if(std::cin.eof()) {
        std::cout << "Saving the game.." << std::endl;
        delete [] input;
        return nullptr;
    }

    //allocating return int array and inserting correct input
    int * finalInput = new int [4];
    finalInput[0] = (int)(input[0]-INT_ASCII_OFFSET);
    finalInput[1] = (int)(input[1]-INT_ASCII_OFFSET);
    finalInput[2] = (int)(input[2]-INT_ASCII_OFFSET);
    finalInput[3] = (int)(input[3]-INT_ASCII_OFFSET);

    delete [] input;
    return finalInput;
}

//special rules are highlighted
/** Designed in way that recieved move is already verified and simply executes it (with all requisites)*/
Chessboard Chessboard::executeMove(const int x1, const int y1, const int x2, const int y2, bool outputEnabled) {
    if(!outputEnabled) {
        silenceOutput();
    }
    unsilencingAfterReturn uar;
    
    if(square[x1][y1].getType() == 'K') {
        //king variables managment .. move with king -> castling not available anymore, and if we are able to move the king, he wont be in check anymore
        if(playerTurn) {
            wKingPos[0] = x2;
            wKingPos[1] = y2;
            whiteCastleAv[0] = false;
            whiteCastleAv[1] = false;
            wKingInCheck = false;
        } else {
            bKingPos[0] = x2;
            bKingPos[1] = y2;
            blackCastleAv[0] = false;
            blackCastleAv[1] = false;
            bKingInCheck = false;
        }

        //*************************************************************
        //  CASTLING
        //if flag is set, castling is correctly called and we need to move rook aswell
        int castlingFlag = setCastlingFlag(x1,y1,x2,y2);
        if (castlingFlag != 0) {
            switch (castlingFlag) {
                case 1:
                    castlingFlag = 0;
                    executeMove(0,0,3,0,outputEnabled);
                    break;
                case 2:
                    castlingFlag = 0;
                    executeMove(7,0,5,0,outputEnabled);
                    break;
                case 3:
                    castlingFlag = 0;
                    executeMove(0,7,3,7,outputEnabled);
                    break;
                case 4:
                    castlingFlag = 0;
                    executeMove(7,7,5,7,outputEnabled);
                    break;
                
                default:
                    break;
            }
            //must be called once again because output is re-enabled after exiting executeMove of rook move
            if(!outputEnabled) {
                silenceOutput();
            }
        }
        //*************************************************************
      //another castling managment, if rook was moved, one castling options is not available anymore
    } else if (square[x1][y1].getType() == 'R') {
        if(x1 == 0) {
            if(y1 == 0) {
                whiteCastleAv[0] = false;
            } else 
                blackCastleAv[0] = false;
        } else {
            if(y1 == 0) {
                whiteCastleAv[1] = false;
            } else
                blackCastleAv[1] = false;
        }
    }

    //*************************************************************
    // EN PASSANT 
    else if((square[x1][y1].getType() == 'p' && x1 != x2) && !square[x2][y2].isOccupied()) {
        if(y1 == 4) { // maybe i dont need these if/if else
            removedPieces.push_back(square[x2][y1].getPiece());
            square[x2][y1].removePiece();       
        } else if (y1 == 3) {
            removedPieces.push_back(square[x2][y1].getPiece());
            square[x2][y1].removePiece();
        }
        std::cout << "*en passant*" << std::endl; 
    }
    //************************************************************

    //move and clear
    if(square[x2][y2].isOccupied()) {
        if(square[x2][y2].getColor())
            std::cout << "# "  <<  square[x2][y2].getType() << " was removed!" << std::endl;
        else
            std::cout << "# ." << square[x2][y2].getType() << " was removed!" << std::endl;
        removedPieces.push_back(square[x2][y2].getPiece());
        square[x2][y2].removePiece();
    }
    //outputting change so player can be sure what has been moved
    if(square[x1][y1].getColor())
        std::cout << "#  " <<  square[x1][y1].getType() << " moved from [" << (char)(x1 + CHAR_ASCII_OFFSET) << y1 << "] to ["<< (char)(x2 + CHAR_ASCII_OFFSET) << y2 << "]" << std::endl;
    else
        std::cout << "# ." <<  square[x1][y1].getType() << " moved from [" << (char)(x1 + CHAR_ASCII_OFFSET) << y1 << "] to ["<< (char)(x2 + CHAR_ASCII_OFFSET) << y2 << "]" << std::endl;

    square[x2][y2].setPiece(square[x1][y1].getPiece());
    square[x1][y1].removePiece();

    //************************************************************
    // PAWN TO QUEEN
    if(square[x2][y2].getType() == 'p' && (y2 == BOARD_Y_MIN || y2 == BOARD_Y_MAX)) {
        bool pawnColor = square[x2][y2].getColor();
        square[x2][y2].removePiece();
        square[x2][y2].setPiece(new Queen(pawnColor)); 
        std::cout << "#  -> and was promoted to Queen!\n";
    }
    //************************************************************

    lastMove[0] = x1;
    lastMove[1] = y1;
    lastMove[2] = x2;
    lastMove[3] = y2;

    return *this;
}

/** Generates all correct possible moves for specific player*/
std::map<coordinates,std::vector<coordinates>> Chessboard::validMovesGen(bool & player) const {
    std::map<coordinates,std::vector<coordinates>> allValidMoves;
    //loop through the chessboard
    for (int x = BOARD_X_MIN; x <= BOARD_X_MAX; ++x) {
        for(int y = BOARD_Y_MIN; y <= BOARD_Y_MAX; ++y) {
            int count = 0;
            std::vector<coordinates> possMove;
            //looking for occupied squares with pieces by specified player
            if(square[x][y].isOccupied()) {
                if(square[x][y].getColor() == player) {
                    //looping through the chessboard for possible moves
                    for(int ix = BOARD_X_MIN; ix <= BOARD_X_MAX; ++ix) {
                        for(int iy = BOARD_Y_MIN; iy <= BOARD_Y_MAX; ++iy) {
                            if(tryMove(x,y,ix,iy,false) && !wouldKingGetInCheck(*this,x,y,ix,iy)) {
                                count++;
                                possMove.push_back(coordinates(ix,iy));
                            }
                        }
                    }
                    //if atleast one move was found, push_back it to result
                    if(count)
                        allValidMoves.insert(std::pair<coordinates,std::vector<coordinates>>(coordinates(x,y), possMove));
                }
            }
        }
    }
    return allValidMoves;
}

/** Evaluate value of figure on specific position, used in evaluate board for better AI calculations*/
int Chessboard::getPositionFigureValue(coordinates pos, const bool & player) const {
    switch(square[pos.m_x][pos.m_y].getType()) {
        case 'B':
            //x is same for both players but map needs to be mirrorly (yes i came up with this word) reversed
            pos.m_y = player ? pos.m_y : BOARD_Y_MAX - pos.m_y;
            return bishopBoardValues->at(pos.m_x).at(pos.m_y);
            break;
        case 'K':
            pos.m_y = player ? pos.m_y : BOARD_Y_MAX - pos.m_y;
            return kingBoardValues->at(pos.m_x).at(pos.m_y);
            break;
        case 'N':
            pos.m_y = player ? pos.m_y : BOARD_Y_MAX - pos.m_y;
            return knightBoardValues->at(pos.m_x).at(pos.m_y);
            break;
        case 'p':
            pos.m_y = player ? pos.m_y : BOARD_Y_MAX - pos.m_y;
            return pawnBoardValues->at(pos.m_x).at(pos.m_y);
            break;
        case 'Q':
            pos.m_y = player ? pos.m_y : BOARD_Y_MAX - pos.m_y;
            return queenBoardValues->at(pos.m_x).at(pos.m_y);
            break;
        case 'R':
            pos.m_y = player ? pos.m_y : BOARD_Y_MAX - pos.m_y;
            return rookBoardValues->at(pos.m_x).at(pos.m_y);
            break;
        default:
            return 0;
            break;
    }
}

/** Evaluates board and result is used in ChessAI to calculare minimax */
int Chessboard::evaluateBoard() const {
    bool white = true;
    bool black = false;
    auto whiteMoves = validMovesGen(white);
    auto blackMoves = validMovesGen(black);

    int score = 0;

    //score of material
    for (int x = BOARD_X_MIN; x <= BOARD_X_MAX; ++x) {
        for(int y = BOARD_Y_MIN; y <= BOARD_Y_MAX; ++y) {
            if(square[x][y].isOccupied())
                score += square[x][y].getPiece()->getValue();
        }
    }
    
    //score of positioning
    for (auto it = whiteMoves.begin(); it != whiteMoves.end(); ++it) {
        //score += it->second.size();
        score -= getPositionFigureValue(it->first, white);
    }

    for (auto it = blackMoves.begin(); it != blackMoves.end(); ++it) {
        //score -= it->second.size();
        score += getPositionFigureValue(it->first, black);
    }

    return score;
}

//loop may be confusing because the switch of i and j and i decrement but
//in data board is represented that figures are set on the sides and not top and bottom
//but that would be very confusing
/** Draws the board with figures and the legend next to it*/
void Chessboard::printBoard() const {
    char print = 'A';
    std::cout << std::endl;
    for (int i = BOARD_X_MAX; i >= BOARD_X_MIN; --i) {
        std::cout << " " << i << " ";
        for(int j = BOARD_Y_MIN; j <= BOARD_Y_MAX; ++j) {
            //no piece set
            if(!square[j][i].isOccupied())
                std::cout << "[   ]";
            //piece set on curr square
            else {
                bool color = square[j][i].getColor();
                if (color) {
                    std::cout << "[ " << square[j][i].getType() << " ]";
                } else
                    std::cout << "[." << square[j][i].getType() << " ]";
            }
        }
        printLegend(i);
        std::cout << std::endl;
    }
    //prints letter under the Chessboard
    std::cout << "     ";
    for ( int i = BOARD_X_MIN; i <= BOARD_X_MAX; ++i) {
        std::cout << (char)(print + i) << "    ";
    }
    std::cout << std::endl << std::endl;
}

/** Used in printBoard to output legend*/
void Chessboard::printLegend(int row) const {
    switch(row) {
        case 7:
            std::cout << "\t# Figures legend:";
            break;
        case 6:
            std::cout << "\t  -  B|.B = Bishop";
            break;
        case 5:
            std::cout << "\t  -  K|.K = King";
            break;
        case 4:
            std::cout << "\t  -  N|.N = Knight";
            break;
        case 3:
            std::cout << "\t  -  p|.p = Pawn";
            break;
        case 2:
            std::cout << "\t  -  Q|.Q = Queen";
            break;
        case 1:
            std::cout << "\t  -  R|.R = Rook";
            break;
        case 0:
            std::cout << "\t(white|.black)";
            break;
        default:
            break;
    }
}

/** Calculates if the match is over by any end-rule*/
bool Chessboard::isGameOver(bool outputEnabled) const {
    bool white = true;
    bool black = false;
    
    if(isKingInCheck(white) && !canKingGetOutOfCheck(white)) {
        if(outputEnabled)
            std::cout << "Checkmate! -> Black wins!" << std::endl;
        return true;
    }
    if(isKingInCheck(black) && !canKingGetOutOfCheck(black)) {
        if(outputEnabled)
            std::cout << "Checkmate! -> White wins!" << std::endl;
        return true;
    }
    if(!validMovesGen(white).size() || !validMovesGen(black).size()) {
        if(outputEnabled)   
            std::cout << "Stalemate! -> Draw!" << std::endl;
        return true;
    }

    return false;
}

/** Creates copy of chessboard, test the move and executes is -> if king of player whose turn is would get in check return true */
bool Chessboard::wouldKingGetInCheck(Chessboard test, const int x1, const int y1, const int x2, const int y2) const {
    //if move is not legit, return
    if(!test.tryMove(x1,y1,x2,y2,false)) {
        return false;
    }
    test.executeMove(x1,y1,x2,y2,false);


    //getting position of the right-color king
    int kingX;
    int kingY;
    if(test.playerTurn) {
        kingX = test.wKingPos[0];
        kingY = test.wKingPos[1];
    } else {
        kingX = test.bKingPos[0];
        kingY = test.bKingPos[1];
    }

    //loop through the whole board if any enemy piece can reach king(is attacking the king)
    for ( int i = BOARD_X_MAX; i >= BOARD_X_MIN; --i) {
        for(int j = BOARD_Y_MIN; j <= BOARD_Y_MAX; ++j) {
            if(test.tryMove(j, i, kingX, kingY,false)) {
                return true;
            }
        }
    }
    return false;
}

/** Checks if king of given player is in check*/
bool Chessboard::isKingInCheck (bool player) const {
    //puts cerr into quiet mode (discards any output) -- disabling
    silenceOutput();
    
    if(player) {
        for ( int i = BOARD_X_MAX; i >= BOARD_X_MIN; --i) {
            for(int j = BOARD_Y_MIN; j <= BOARD_Y_MAX; ++j) {
                bool wking = tryMove(j, i, wKingPos[0], wKingPos[1],false);
                if(wking) {
                    unsilenceOutput();  
                    return true;
                }
            }
        }
    } else {
        for ( int i = BOARD_X_MAX; i >= BOARD_X_MIN; --i) {
            for(int j = BOARD_Y_MIN; j <= BOARD_Y_MAX; ++j) {
                bool bking = tryMove(j, i, bKingPos[0], bKingPos[1],false);
                if(bking) {
                    unsilenceOutput();  
                    return true;
                }
            }
        }
    }
    
    //--enabling back
    unsilenceOutput();
    return false;
}

/** Designed to be called after knowing that specified King is in check and calculates if he can get out of it */
bool Chessboard::canKingGetOutOfCheck(bool player) const {
    std::cerr.setstate(std::ios_base::failbit);
    //if move is not possible or king would get in check, we have to check if we can get rid of attacker
    //otherwise its a checkmate
    if(player) {
        //checking if king can move by itself without getting in check
        if ((!tryMove(wKingPos[0], wKingPos[1], wKingPos[0] - 1, wKingPos[1] + 1,false) || wouldKingGetInCheck(*this,wKingPos[0], wKingPos[1], wKingPos[0] - 1, wKingPos[1] + 1))     &&
            (!tryMove(wKingPos[0], wKingPos[1], wKingPos[0], wKingPos[1] + 1,false)     || wouldKingGetInCheck(*this,wKingPos[0], wKingPos[1], wKingPos[0], wKingPos[1] + 1))         &&
            (!tryMove(wKingPos[0], wKingPos[1], wKingPos[0] + 1, wKingPos[1] + 1,false) || wouldKingGetInCheck(*this,wKingPos[0], wKingPos[1], wKingPos[0] + 1, wKingPos[1] + 1))     &&
            (!tryMove(wKingPos[0], wKingPos[1], wKingPos[0] + 1, wKingPos[1],false)     || wouldKingGetInCheck(*this,wKingPos[0], wKingPos[1], wKingPos[0] + 1, wKingPos[1]))         &&
            (!tryMove(wKingPos[0], wKingPos[1], wKingPos[0] + 1, wKingPos[1] - 1,false) || wouldKingGetInCheck(*this,wKingPos[0], wKingPos[1], wKingPos[0] + 1, wKingPos[1] - 1))     &&
            (!tryMove(wKingPos[0], wKingPos[1], wKingPos[0], wKingPos[1] - 1,false)     || wouldKingGetInCheck(*this,wKingPos[0], wKingPos[1], wKingPos[0], wKingPos[1] - 1))         &&
            (!tryMove(wKingPos[0], wKingPos[1], wKingPos[0] - 1,wKingPos[1] - 1,false)  || wouldKingGetInCheck(*this,wKingPos[0], wKingPos[1], wKingPos[0] - 1,wKingPos[1] - 1))      &&
            (!tryMove(wKingPos[0], wKingPos[1], wKingPos[0] - 1,wKingPos[1],false)      || wouldKingGetInCheck(*this,wKingPos[0], wKingPos[1], wKingPos[0] - 1,wKingPos[1]))) {
            //king cant move so we must check, if we can get the attacker    
            int attackerX = -1; 
            int attackerY = -1;

            //getting attacker position
            for ( int i = BOARD_X_MAX; i >= BOARD_X_MIN; --i) {
                for(int j = BOARD_Y_MIN; j <= BOARD_Y_MAX; ++j) {
                    if(tryMove(j, i, wKingPos[0], wKingPos[1],false)) {
                        attackerX = j;
                        attackerY = i;
                        break;
                    }
                }
                if ( attackerX != -1)
                    break;
            }

            //check if attacker can be cleared
            for ( int i = BOARD_X_MAX; i >= BOARD_X_MIN; --i) {
                for(int j = BOARD_Y_MIN; j <= BOARD_Y_MAX; ++j) {
                    if(tryMove(j, i, attackerX, attackerY,false)){
                        std::cerr.clear();
                        return true;
                    }
                }
            }
            std::cerr.clear();
            return false;

        } else {
            std::cerr.clear();
            return true;
        }
    } else {
        //checking if king can move by itself without getting in check
        if( (!tryMove(bKingPos[0], bKingPos[1], bKingPos[0] - 1, bKingPos[1] + 1,false) || wouldKingGetInCheck(*this, bKingPos[0], bKingPos[1], bKingPos[0] - 1, bKingPos[1] + 1))   &&
            (!tryMove(bKingPos[0], bKingPos[1], bKingPos[0], bKingPos[1] + 1,false)     || wouldKingGetInCheck(*this, bKingPos[0], bKingPos[1], bKingPos[0], bKingPos[1] + 1))       &&
            (!tryMove(bKingPos[0], bKingPos[1], bKingPos[0] + 1, bKingPos[1] + 1,false) || wouldKingGetInCheck(*this, bKingPos[0], bKingPos[1], bKingPos[0] + 1, bKingPos[1] + 1))   &&
            (!tryMove(bKingPos[0], bKingPos[1], bKingPos[0] + 1, bKingPos[1],false)     || wouldKingGetInCheck(*this, bKingPos[0], bKingPos[1], bKingPos[0] + 1, bKingPos[1]))       &&
            (!tryMove(bKingPos[0], bKingPos[1], bKingPos[0] + 1, bKingPos[1] - 1,false) || wouldKingGetInCheck(*this, bKingPos[0], bKingPos[1], bKingPos[0] + 1, bKingPos[1] - 1))   &&
            (!tryMove(bKingPos[0], bKingPos[1], bKingPos[0], bKingPos[1] - 1,false)     || wouldKingGetInCheck(*this, bKingPos[0], bKingPos[1], bKingPos[0], bKingPos[1] - 1))       &&
            (!tryMove(bKingPos[0], bKingPos[1], bKingPos[0] - 1,bKingPos[1] - 1,false)  || wouldKingGetInCheck(*this, bKingPos[0], bKingPos[1], bKingPos[0] - 1,bKingPos[1] - 1))    &&
            (!tryMove(bKingPos[0], bKingPos[1], bKingPos[0] - 1,bKingPos[1],false)      || wouldKingGetInCheck(*this, bKingPos[0], bKingPos[1], bKingPos[0] - 1,bKingPos[1]))) {
            //king cant move so we must check, if we can get the attacker    
            int attackerX = -1; 
            int attackerY = -1;
            
            //getting attacker position
            for ( int i = BOARD_X_MAX; i >= BOARD_X_MIN; --i) {
                for(int j = BOARD_Y_MIN; j <= BOARD_Y_MAX; ++j) {
                    if(tryMove(j, i, bKingPos[0], bKingPos[1],false)) {
                        attackerX = j;
                        attackerY = i;
                        break;
                    }
                }
                if ( attackerX != -1)
                    break;
            }

            //check if attacker can be cleared
            for ( int i = BOARD_X_MAX; i >= BOARD_X_MIN; --i) {
                for(int j = BOARD_Y_MIN; j <= BOARD_Y_MAX; ++j) {
                    if(tryMove(j, i, attackerX, attackerY,false) && !wouldKingGetInCheck(*this,j, i, attackerX, attackerY)){
                        std::cerr.clear();
                        //std::cout << "yeah attacker can be cleared\n";
                        return true;
                    }
                }
            }
            std::cerr.clear();
            return false;

        } else {
            std::cerr.clear();
            return true;
        }            
    }
}

/**
 *  Checks if specified castling is currently available
 *  CASTLING FLAGS: (left/right is from perspective of white player)
 *  1 - left white castling
 *  2 - right white castling
 *  3 - left black castling
 *  4 - right black castling
 */
bool Chessboard::isCastlingPossile(int flag) const {
    switch (flag) {
    //checks if castling is available(figures were not moved yet) and if there are none pieces between the king and rook,
    //if yes, flag is set and will be executed in executeMove
    case 1:
        if(whiteCastleAv[0] && !square[1][0].isOccupied() && !square[2][0].isOccupied() && !square[3][0].isOccupied()) {
            //castlingFlag = 1;
            return true;
        } else 
            std::cerr << "castling not available by now\n";
        break;
    case 2:
        if(whiteCastleAv[1] && !square[5][0].isOccupied() && !square[6][0].isOccupied()) {
            //castlingFlag = 2;
            return true;
        } else 
            std::cerr << "castling not available by now\n";
        break;
    case 3:
        if(blackCastleAv[0] && !square[1][7].isOccupied() && !square[2][7].isOccupied() && !square[3][7].isOccupied()) {
            //castlingFlag = 3;
            return true;
        } else 
            std::cerr << "castling not available by now\n";
        break;
    case 4:
        if(blackCastleAv[1] && !square[5][7].isOccupied() && !square[6][7].isOccupied()) {
            //castlingFlag = 4;
            return true;
        } else 
            std::cerr << "castling not available by now\n";
        break;
    default:
        break;
    }
    return false;
}

/** Method that decides which castling was called, correctness was tested in tryMove */
//was implemented so tryMove can be constant function and many others that use tryMove because flags were called in tryMove
int Chessboard::setCastlingFlag(const int & x1, const int & y1, const int & x2, const int & y2) const {
    //whiteKing
    if((x1 == 4 && y1 == 0)) {
        //left castling
        if(x2 == 2 && y2 == 0) {
            return 1;
        //right castling
        } else if(x2 == 6 && y2 == 0) {
            return 2;
        }
    //blackKing
    } else if((x1 == 4 && y2 == 7)) {
        //left castling
        if(x2 == 2 && y2 == 7) {
            return 3;
        //right castling
        } else if(x2 == 6 && y2 == 7) {
            return 4;
        }
    }
    return 0;
}

/** Initialization of value vectors that represents maps of each figure and positions that indicates good/bad position */
void Chessboard::initValueVectors() {
    bishopBoardValues = new std::vector<std::vector <int>> {
        {-2,-1,-1,-1,-1,-1,-1,-2},
        {-1,1,1,0,0,0,0,-1},
        {-1,0,1,1,0,0,0,-1},
        {-1,0,1,1,1,1,0,-1},
        {-1,0,1,1,1,1,0,-1},
        {-1,0,1,1,0,0,0,-1},
        {-1,1,1,0,0,0,0,-1},
        {-2,-1,-1,-1,-1,-1,-1,-2}      
    };
    
    kingBoardValues = new std::vector<std::vector <int>> {
        {2,2,-1,-2,-3,-3,-3,-3},
        {3,2,-2,-3,-4,-4,-4,-4},
        {1,0,-2,-3,-4,-4,-4,-4},
        {0,0,-2,-3,-4,-4,-4,-4},
        {0,0,-2,-3,-4,-4,-4,-4},
        {1,0,-2,-3,-4,-4,-4,-4},
        {3,2,-2,-3,-4,-4,-4,-4},
        {2,2,-1,-2,-3,-3,-3,-3}
    };

    knightBoardValues = new std::vector<std::vector <int>> {
        {-5,-4,-3,-3,-3,-3,-4,-5},
        {-4,-2,0,0,1,0,-2,-4},
        {-3,0,1,2,2,1,0,-3},
        {-3,1,2,3,3,2,0,-3},
        {-3,1,2,3,3,2,0,-3},
        {-3,0,1,2,2,1,0,-3},
        {-4,-2,0,0,1,0,-2,-4},
        {-5,-4,-3,-3,-3,-3,-4,-5}
    };

    pawnBoardValues = new std::vector<std::vector <int>> {
        {0,1,1,0,1,1,5,0},
        {0,1,-1,0,1,1,5,0},
        {0,1,-1,0,2,2,5,0},
        {0,-2,0,2,3,3,5,0},
        {0,-2,0,2,3,3,5,0},
        {0,1,-1,0,2,2,5,0},
        {0,1,-1,0,1,1,5,0},
        {0,1,1,0,1,1,5,0}
    };

    queenBoardValues = new std::vector<std::vector <int>> {
        {-2,-1,-1,0,0,-1,-1,-2},
        {-1,0,1,0,0,0,0,-1},
        {-1,0,1,0,0,1,0,-1},
        {0,0,0,1,1,0,0,0},
        {0,0,0,1,1,0,0,0},
        {-1,0,1,0,0,1,0,-1},
        {-1,0,1,0,0,0,0,-1},
        {-2,-1,-1,0,0,-1,-1,-2}
    };

    rookBoardValues = new std::vector<std::vector <int>> {
        {0,-1,-1,-1,0,0,1,0},
        {0,0,0,0,0,0,1,0},
        {0,0,0,0,0,0,1,0},
        {1,0,0,0,0,0,1,0},
        {1,0,0,0,0,0,1,0},
        {0,0,0,0,0,0,1,0},
        {0,0,0,0,0,0,1,0},
        {0,-1,-1,-1,0,0,1,0}
    };
}

/** Deletes pointers to vectors so they will get deleted by themself*/
void Chessboard::freeValueVector() {
    delete bishopBoardValues;
    delete kingBoardValues;
    delete knightBoardValues;
    delete pawnBoardValues;
    delete queenBoardValues;
    delete rookBoardValues;
}