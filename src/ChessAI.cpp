#include "ChessAI.hpp"

const int INT_MAX = 2147400000;
const int INT_MIN = -2147400000;

/**Produces random move from all possible correct moves and executes it*/
void ChessAI::playRandom(Chessboard & chessboard) const {
    srand (time(NULL));
    //getting all possible moves
    auto possMoves = chessboard.validMovesGen(chessboard.playerTurn);

    //selectiong random piece
    size_t randPiece = rand() % possMoves.size();

    coordinates input(0,0);
    std::vector<coordinates> indx;
    for (auto it = possMoves.begin(); it != possMoves.end(); ++it) {
        if(!randPiece) {
            input = it->first;
        }
        randPiece--;        
    }

    //selecting random move
    size_t randMove = rand() % possMoves.at(input).size();
    coordinates output(0,0);
    for (auto it = possMoves.at(input).begin(); it != possMoves.at(input).end(); ++it) {
        if(!randMove) {
            output = *it;
        }
        randMove--;        
    }
    system("clear");
    chessboard.executeMove(input.m_x,input.m_y,output.m_x,output.m_y);
}

/**Produces and executes best possible move calculated by minimax algorithm*/
void ChessAI::playMinimax(Chessboard & chessboard) const {
    //calculating
    std::pair<coordinates,coordinates> bestMove = findBestMove (chessboard, difficulty);
    system("clear");
    //executing
    chessboard.executeMove(bestMove.first.m_x,bestMove.first.m_y,bestMove.second.m_x,bestMove.second.m_y);
}

/**Calculating best possible move by given depth, its designed for AI that plays black side and tries to MAXIMAZE*/
std::pair<coordinates,coordinates> ChessAI::findBestMove (Chessboard chessboard, int depth) const {
    bool black = false;

    //setting "bestValue" to opposite whats best, resp. its the worst
    int bestValue = INT_MIN;
    std::pair<coordinates,coordinates> bestMove;

    //getting all possible moves
    auto moves = chessboard.validMovesGen(black);
    //going through all specified player figures
    for ( auto itFig = moves.begin(); itFig != moves.end(); ++itFig) {
        //going through all moves
        for( auto itMov = itFig->second.begin(); itMov != itFig->second.end(); ++itMov) {
            Chessboard next = chessboard;
            //minimax search for best value
            int val = minimax(next.executeMove(itFig->first.m_x,itFig->first.m_y, itMov->m_x,itMov->m_y, false), depth, INT_MIN, INT_MAX, true);
            //if we find better value, change it and set the best move
            if(val > bestValue) {
                bestValue = val;
                bestMove.first.m_x = itFig->first.m_x;
                bestMove.first.m_y = itFig->first.m_y;
                bestMove.second.m_x = itMov->m_x;
                bestMove.second.m_y = itMov->m_y;
            }
        }
    }
    return bestMove;
}

/**Minimax algorithm.. each step explained at each code line*/
int ChessAI::minimax (Chessboard chessboard, int depth, int alpha, int beta, bool maximizingPlayer) const {
    //if we reached given depth or game is in gameOver stage evaluate board
    if(depth == 0 || chessboard.isGameOver())
        return chessboard.evaluateBoard();

    //maximizing player that wants the board to have the most positive value possible
    if(maximizingPlayer){
        bool maximizing = true;
        //setting maxVal to worst value possible
        int maxVal = INT_MIN;
        //getting possible moves
        auto possMoves = chessboard.validMovesGen(maximizing);
        
        //going through all specified player figures
        for ( auto itFig = possMoves.begin(); itFig != possMoves.end(); ++itFig) {
            //going through all moves
            for( auto itMov = itFig->second.begin(); itMov != itFig->second.end(); ++itMov) {
                //coppying board so we dont have to undo the changes
                Chessboard next = chessboard;
                int val = minimax(next.executeMove(itFig->first.m_x,itFig->first.m_y,itMov->m_x,itMov->m_y,false),depth - 1, alpha, beta,false);
                //if higher value was found, update maxVal
                maxVal = std::max(maxVal,val);
                //if max value is higher than alpha, update alpha
                alpha = std::max(alpha,val);
                //the minimax is designed that result should be the highest possible for maximizing player, so if 
                //evaluate value from one branch is less than already found max, we dont have to calculate rest of that branch and can skip
                if(beta <= alpha)
                    break;
            }
        }
        return maxVal;
    //minimazing player that wants the board to have the most negative value possible
    } else {
        bool minimazing = false;
        //setting maxVal to worst value possible
        int minVal = INT_MAX;
        //getting possible moves
        auto possMoves = chessboard.validMovesGen(minimazing);

        //going through all specified player figures
        for ( auto itFig = possMoves.begin(); itFig != possMoves.end(); ++itFig) {
            //going through all moves
            for( auto itMov = itFig->second.begin(); itMov != itFig->second.end(); ++itMov) {
                //coppying board so we dont have to undo the changes
                Chessboard next = chessboard;
                int val = minimax(next.executeMove(itFig->first.m_x,itFig->first.m_y,itMov->m_x,itMov->m_y,false),depth - 1, alpha, beta, true);
                //if lower value was found, update minVal
                minVal = std::min(minVal,val);
                //if min value is less than beta, update beta
                beta = std::min(beta,val);
                //the minimax is designed that result should be the highest possible for maximizing player, so if 
                //evaluate value from one branch is less than already found max, we dont have to calculate rest of that branch and can skip
                if(beta <= alpha)
                    break;
            }
        }
        return minVal;
    }
}