#pragma once

#include <iostream>
#include <cstdio>
#include <vector>
#include <utility>
#include <string>
#include <map>
#include <memory>
#include <type_traits>

#include "coordinates.hpp"
#include "Square.hpp"

//-------UTILITY_FUNCTIONS-------//
//setting failbit and silencing all output, its used for check and checkmate
void silenceOutput();
//if failbit is set and so output silent, set goodbit for enabling output
void unsilenceOutput();

/**
 * Chessboard puts every single element of Chess together (figures,movement,rules etc.)
 * --  my Chessboard consist of rows 0-7 for better and simplier coding
 * --  takes care of pieces on board, collisions, turns, special rules, over of the match, evaluating board
*/

class Chessboard {
    public:
        /**default constructor*/
        Chessboard(){};

        /**set all pieces on the board*/
        void setBoard();

        /**clears up all pieces from board, ment deallocates them*/
        void clearBoard();

        /**vector of all coordinates with pieces, used for saving current game*/
        std::vector<std::pair<coordinates,Figure*>> getAllFigures() const;

        /**updates variables connected to king which dont neccesarily need to be saved and can be read from Chessboard*/
        void updateKingsPos();

        /**after save is loaded correctly, set loaded pieces on board*/
        void setFromLoadFile(std::vector<std::pair<coordinates,Figure*>> loadFile);
        
        /**move validator on chessboard, verifying collisions, special rules moves*/
        bool tryMove(const int x1, const int y1, const int x2, const int y2, bool outputEnabled = true) const;
        
        /**reads move from input, it handles movements out of bounds, move with enemy piece or move to the same square*/
        int * readMove() const;

        /**after move was succesfuly tested execute it (meant set and remove pieces)*/
        Chessboard executeMove(const int x1, const int y1, const int x2, const int y2, bool outputEnabled = true);

        /**function that generates all valid moves of specified player, its used for stalemate
        and for AI minimax needs... it returns map with key (coordinates of figure) and value (possible moves of that figure)*/
        std::map<coordinates,std::vector<coordinates>> validMovesGen(bool & player) const;

        /**evaluate value of figure on specific position, used in evaluate board for better AI calculations*/
        int getPositionFigureValue(coordinates pos, const bool & player) const;
        /**evaluate board score*/
        int evaluateBoard() const;

        /**prints out board with pieces currently on it*/
        void printBoard() const;

        /**switch between white/black turns*/
        void changePlayerTurn() { 
            playerTurn = !playerTurn; 
            return;
        };

        /**game over check, if any game-ending condition is met, it couts correct output with result of match*/
        bool isGameOver(bool outputEnabled = false) const;
        
        /**functions that keeps track of checks/mate and castling, functionality in cpp*/
        bool wouldKingGetInCheck(Chessboard test, const int x1, const int y1, const int x2, const int y2) const;
        bool isKingInCheck(bool player) const;
        bool canKingGetOutOfCheck( bool player) const;
        bool isCastlingPossile(int flag) const;
        int  setCastlingFlag(const int & x1,const int & y1,const int & x2,const int & y2) const;

        /**memory managment of value boards for each figure, they are only initialized in matches versus AI*/
        void initValueVectors();
        void freeValueVector();
        
        /**true -> white turn | false -> black turn*/
        bool playerTurn;

        /**utility variable*/
        bool gameRunning;
    private:
        /**representation of the board*/
        Square square[8][8];
        std::vector<Figure *> removedPieces;
        
        /**variables for easier check/mate tracking*/
        int wKingPos[2];
        int bKingPos[2];
        bool wKingInCheck;
        bool bKingInCheck;

        /**variables for castling, */
        bool whiteCastleAv[2] = {true,true};
        bool blackCastleAv[2] = {true,true};

        /**vectors of values that adds up to specific piece on specific position calculated in evaluateBoard, initialized to nullptr
        because they are needed only versus AI, used pointer so we dont need to copy data each time we copy Chessboard*/
        std::vector < std::vector <int>> * bishopBoardValues = nullptr;
        std::vector < std::vector <int>> * kingBoardValues = nullptr;
        std::vector < std::vector <int>> * knightBoardValues = nullptr;
        std::vector < std::vector <int>> * pawnBoardValues = nullptr;
        std::vector < std::vector <int>> * queenBoardValues = nullptr;
        std::vector < std::vector <int>> * rookBoardValues = nullptr;

        /**remembering last move for en passant and save/load game*/
        int lastMove[4] = {0,0,0,0};

        /**method that is used in printBoard() that extends game with legend of figures*/
        void printLegend(int row) const;
        
        /**
         * struct that unsilence output from scope if it was silenced after leaving the scope
         */
        struct unsilencingAfterReturn {
            ~unsilencingAfterReturn() {
                unsilenceOutput();
            }
        };
};