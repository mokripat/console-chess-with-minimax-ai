#include "Game.hpp"

const int INT_ASCII_OFFSET = 48;

/** Starts up new game with given settings*/
void Game::newGame(bool opponent, int difficulty) {
    Chessboard newOne;
    newOne.setBoard();
    currGame = newOne;
    gameRunning = true;
    
    //player versus player mode
    if(!opponent) {
        gamemode = 0;
        playVersus();
      //versus pc mode
    } else {
        if(!difficulty) {
            gamemode = 1;
            playRandom();
        } else {
            gamemode = 1 + difficulty;
            playAI(difficulty);
        }
    }
    //clear the board -> delete all allocated memory(figures)
    currGame.clearBoard();
    return;
}

/** Load up game from specified save file*/
bool Game::loadGame(std::string loadFileName) {
    Chessboard loadedBoard;
    
    //opening the file .. if there is problem, first read will capture it and set corrupt flag
    std::ifstream loadFile(loadFileName);
    
    std::vector<std::pair<coordinates,Figure*>> loadedFigures;
    coordinates tmpCoordinates;

    char tmp_char;
    bool color;
    //getting the code that would be later verified
    std::string code;
    std::getline(loadFile,code);
    code.push_back('\n');

    //getting whose turn was and gamemode
    loadFile >> loadedBoard.playerTurn >> gamemode;
    if(!loadFile) {
        corruptFlag = true;
    } else if(gamemode < 0 || gamemode > 3) {
        corruptFlag = true;
    }

    ///loading figures, their position and color.. //else <- these are for debugging purposes
    while(!loadFile.eof() && !corruptFlag) {
        //x coordinate
        loadFile >> tmpCoordinates.m_x;
        if(tmpCoordinates.m_x < 0 || tmpCoordinates.m_x > 7 || !loadFile) {
            corruptFlag = true;
            break;
        } //else std::cout << tmpCoordinates.m_x << " ";

        //y coordinate
        loadFile >> tmpCoordinates.m_y;
        if(tmpCoordinates.m_y < 0 || tmpCoordinates.m_y > 7 || !loadFile) {
            corruptFlag = true;
            break;
        } //else std::cout << tmpCoordinates.m_y << " ";

        //figure type
        loadFile >> tmp_char;
        if((tmp_char != 'p' && tmp_char != 'R' && tmp_char != 'N' && tmp_char != 'B' && tmp_char != 'Q' && tmp_char != 'K') || !loadFile) {
            corruptFlag = true;
            break;
        } //else std::cout << tmp_char << " ";

        //figure color
        loadFile >> color;
        if(!loadFile) {
            corruptFlag = true;
            break;
        } //else std::cout << color << "\n";
        //push_back the loaded figure to result
        //for first figure
        if(!loadedFigures.size())
            loadedFigures.push_back(std::make_pair(tmpCoordinates,createPiece(tmp_char,color)));
        else if(!(tmpCoordinates.m_x == loadedFigures.back().first.m_x && tmpCoordinates.m_y == loadedFigures.back().first.m_y))
            loadedFigures.push_back(std::make_pair(tmpCoordinates,createPiece(tmp_char,color)));
    }

    //if anything went wrong, output wrong file
    if(corruptFlag) {
        std::cerr << "Loadfile is corrupted or was changed..\n";

        //delete loaded(allocated) figure
        for (auto it = loadedFigures.begin(); it != loadedFigures.end(); ++it) {
            delete it->second;
        } 
        return false;
    }

    //empty save file is wrong save file
    if(!loadedFigures.size()) {
        std::cerr << "Empty save file..\n"; 
        return false;
    }

    //code verification, that all we loaded is correct
    if (generateSaveCode(loadedFigures).compare(code)) {
        std::cerr << "Verification codes does not match! Save file was modified\n";
        std::cerr << generateSaveCode(loadedFigures) << code;
        
        //delete loaded(allocated) figure
        for (auto it = loadedFigures.begin(); it != loadedFigures.end(); ++it) {
            delete it->second;
        } 
        return false;
    }

    //setup the chessboard with all loaded information
    loadedBoard.setFromLoadFile(loadedFigures);
    setUpChessboard(loadedBoard);

    //run the loaded game
    switch (gamemode) {
        case 0:
            playVersus();
            break;
        case 1:
            playRandom();
            break;
        case 2:
            playAI(1);
            break;
        case 3:
            playAI(2);
            break;
        
        default:
            break;
    }
    currGame.clearBoard();
    return true;
}


/** Saves the current match to saveFolder*/
void Game::saveGame() const {
    //finding the next save file name
    int i = 1;
    std::string saveFileName;
    while (true) {
        saveFileName = "saves/save";
        saveFileName.push_back((i+INT_ASCII_OFFSET));
        
        struct stat buffer;
        if(stat (saveFileName.c_str(), &buffer) != 0) {
            break;
        } else 
            i++;
    }
    //this version supports 9 saves, every other will be saved as save1 but you can delete all saves in game menu folder
    if(i > 9) {
        saveFileName = "saves/save1";
        std::cout << "Too much saved games, first save will be overwritten\n";
    }

    system("mkdir -p saves");

    std::ofstream newSafeFile(saveFileName);
    auto figureList = currGame.getAllFigures();

    //encoding
    newSafeFile << generateSaveCode(figureList);

    //remembering player turn and gamemode
    newSafeFile << currGame.playerTurn << " " << gamemode << std::endl;

    //write figures to the save file one by one
    for(auto it = figureList.begin(); it != figureList.end(); ++it) {
        newSafeFile << it->first.m_x << " " << it->first.m_y << " " << it->second->getType() << " " << it->second->getColor() << std::endl;
    }
    //confirmation for user
    std::cout << "Match saved as " << saveFileName << std::endl;
    return;
}

/** Starts and take care of match player versus player */
void Game::playVersus() {
    system("clear");
    currGame.printBoard();

    while(currGame.gameRunning) {
        //getting input
        int * input = currGame.readMove();
        // EOF/savegame handle
        if (std::cin.eof()) {
            currGame.gameRunning = false;
            saveGame();
            break;
        }
        //trying move from input
        if (!currGame.tryMove(input[0],input[1],input[2],input[3]) || currGame.wouldKingGetInCheck(currGame,input[0],input[1],input[2],input[3])) {
            std::cout << "# Wrong move!" << std::endl;
            continue;
        }
        system("clear");
        //executing the move and deleting input
        currGame.executeMove(input[0],input[1],input[2],input[3]);
        delete [] input;
        //prints changed board and switch player turn
        currGame.printBoard();
        currGame.changePlayerTurn();

        if(currGame.isGameOver(true)) {
            currGame.gameRunning = false;
        }     
    }

    return;
}

/** Starts and take care of match player versus random moves */
void Game::playRandom() {
    ChessAI PC(0);

    system("clear");
    currGame.printBoard();

    while(currGame.gameRunning) {
        if(currGame.playerTurn) {
            //getting input
            int * input = currGame.readMove();
            
            // EOF/savegame handle
            if (std::cin.eof()) {
                currGame.gameRunning = false;
                saveGame();
                break;
            }
            //trying move from input
            if (!currGame.tryMove(input[0],input[1],input[2],input[3]) || currGame.wouldKingGetInCheck(currGame,input[0],input[1],input[2],input[3])) {
                std::cout << "# Wrong move!" << std::endl;
                continue;
            }
            system("clear");
            //executing the move and deleting input
            currGame.executeMove(input[0],input[1],input[2],input[3]);
            delete [] input;
        } else {
            std::cout << "Calculating move.." << std::endl;
            //getting and executing random move
            PC.playRandom(currGame);
        }
        //prints changed board and switch player turn
        currGame.printBoard();
        currGame.changePlayerTurn();

        if(currGame.isGameOver(true)) {
            currGame.gameRunning = false;
        }     
    }

    return;
}

/** Starts and take care of match player versus computer*/
void Game::playAI(int difficulty) {
    //initialization of AI
    ChessAI PC(difficulty);
    currGame.initValueVectors();

    system("clear");
    currGame.printBoard();

    while(currGame.gameRunning) {
        if(currGame.playerTurn) {
            //getting input
            int * input = currGame.readMove();

            // EOF/savegame handle
            if (std::cin.eof()) {
                currGame.gameRunning = false;
                saveGame();
                currGame.freeValueVector();
                break;
            }
            //trying move from input
            if (!currGame.tryMove(input[0],input[1],input[2],input[3]) || currGame.wouldKingGetInCheck(currGame,input[0],input[1],input[2],input[3])) {
                std::cout << "# Wrong move!" << std::endl;
                continue;
            }
            system("clear");
            currGame.executeMove(input[0],input[1],input[2],input[3]);
            delete [] input;
        } else {
            std::cout << "Calculating move.." << std::endl;
            //calculationg best possible move generated from minimax algorithm and executing it
            PC.playMinimax(currGame);
        }
        //prints changed board and switch player turn
        currGame.printBoard();
        currGame.changePlayerTurn();

        if(currGame.isGameOver(true)) {
            currGame.gameRunning = false;
            currGame.freeValueVector();
        }     
    }

    return;
}

/** Allocates and returns specified figure*/
Figure * Game::createPiece(char figure, bool color) const {
    switch (figure) {
    case 'p':
        return new Pawn(color);
    case 'R':
        return new Rook(color);
    case 'N':
        return new Knight(color);
    case 'B':
        return new Bishop(color);
    case 'Q':
        return new Queen(color);
    case 'K':
        return new King(color);
    
    default:
        throw std::runtime_error("Corrupted file..");
        break;
    }
}

/** Handles uninitialized values and set them up for smooth match*/
void Game::setUpChessboard(Chessboard & loaded) {
    loaded.gameRunning = true;
    loaded.updateKingsPos();
    currGame = loaded;
}

/** Generates code for given coordinates with pieces for safe save file having ability being immune for
non-expert sabotage*/
std::string Game::generateSaveCode(std::vector<std::pair<coordinates,Figure*>> figureList) const {
    std::stringstream ss;
    for(auto it = figureList.rbegin(); it != figureList.rend(); ++it) {
        //adding magic constants
        ss << it->first.m_x + 1 << it->first.m_y + 2 << (char)(it->second->getType() + 3) << it->second->getColor();
    }
    ss << std::endl;
    std::string code = ss.str();
    return code;
}