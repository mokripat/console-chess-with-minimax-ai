# Console Chess with minimax AI

Project written in C++ as personal semestral projects that runs in console with multiple modes. It was my first bigger project with very low knowledge of effective algos. Computer AI is done with minimax algo with depth 4. Supports saving and loading.

Documentation to this project is automatically generated as HTML when using `make all` (you need oxygen installed)

Info from landing page of this project in Documentation

# Introduction
This is landing page for game of traditional chess that was created as semestral project for PA2 subject.

## Task (translated to english):
Classic game of Chess
Implement:

    for 2 player on the same PC
    for game versus PC
Games must have these functionalities:

    follow all rules of specific variation (I went for traditional chess) -> more in Chessboard
    save/load match from a file -> more in Game and Chess
    announce the end of the match (check, mate, stalemate) -> more in Chessboard
    artificial inteligence (3 kinds, one can be random) -> more in ChessAI
## Instalation
To compile the game and have everything ready just use:
    `make all`
And to start the aplication:
    `make run`
## Control
### Menu
Firstly you will be able to go through main menu and it's submenus. To navigate type 1-4 representing you choice. You have option to start a brand new game with 4 settings, load up previously saved games (you can make up to 9 saves) or delete all saves to make space for newer ones, show base key instructions or exit the game.

### In-Game
When is your turn, to move a piece, type current position space destination position.

Few correct examples:
    
    a1 a3
    B0 A2
    H0 H7
If you want to save the game press CTRL-D and your game will be automatically saved in saved/ folder.
(To load up the game, re-run the game and choose Load game option in main menu)

## Goal/Objective
To win/end the game, one of the king must be check-mated or stalemated.

## Exceptions/not-intuitive things
In my version of chess there is small expcetion and that is pawn would be always promoted to queen without a choice
Move that would put your King in check is prohibited and game would output wrong move quote
If your King is under the check game wont let you do move that would not get him out of check
## Technicals
### Polymorphism
Polymorphism is used in moveValidator method, which decides if move with some figure would be correct on empty chessboard by traditional rules, its then called in Chessboard class in function tryMove() which is designed in way I dont need to know with which piece I am working, I can get that info easily if I wanted to by calling getter methods or simply call the moveValidator and don't mind.

### Saving the game
Files of saved games are located in saves/ folder and its implemented to have maximum of 9 saves. If more games are saved the first save will be overwritten.

### Loading the game
The game will be looking for save files in saves/ folder and will look for files called save1,save2 etc. Save files have generated code in the first line that keeps files from sabotage.

### Testing the game with examples
To test well functionatily of the game, open mokripat/ folder in terminal and type:

    `make run < examples/$`
    (Where $ is name of test you want to try.)
