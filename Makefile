EXE_FILE_NAME = mokripat 

GCC = g++
GCC_FLAGS = -Wall -g -pedantic -Wextra -std=c++14 -lstdc++fs

BUILD_DIR = build
SAVES_DIR = saves
DOC_DIR = doc

all: compile doc

compile: $(EXE_FILE_NAME)
	@mkdir -p $(SAVES_DIR)
	@echo "Compiling done"

run: compile
	@./$(EXE_FILE_NAME)

doc: src/main.cpp src/coordinates.hpp src/Figures.hpp src/Square.hpp src/Chessboard.hpp src/Game.hpp src/ChessAI.hpp src/Chess.hpp
	doxygen Doxyfile

clean:
	rm -rf $(SAVES_DIR)/ $(DOC_DIR)/ $(EXE_FILE_NAME) $(BUILD_DIR)/ 2>/dev/null

$(EXE_FILE_NAME): $(BUILD_DIR)/main.o $(BUILD_DIR)/coordinates.o $(BUILD_DIR)/Figures.o $(BUILD_DIR)/Square.o $(BUILD_DIR)/Chessboard.o $(BUILD_DIR)/ChessAI.o $(BUILD_DIR)/Game.o $(BUILD_DIR)/Chess.o
	$(GCC) $(GCC_FLAGS) $^ -o $@

$(BUILD_DIR)/%.o: src/%.cpp
	@mkdir -p $(BUILD_DIR)
	$(GCC) $(GCC_FLAGS) $< -c -o $@

# Dependencies
$(BUILD_DIR)/main.o: src/main.cpp src/Chessboard.hpp src/coordinates.hpp src/Square.hpp src/Figures.hpp src/ChessAI.hpp src/Chess.hpp src/Game.hpp
$(BUILD_DIR)/coordinates.o: src/coordinates.cpp src/coordinates.hpp
$(BUILD_DIR)/Figures.o: src/Figures.cpp src/Figures.hpp
$(BUILD_DIR)/Square.o: src/Square.cpp src/Square.hpp src/Figures.hpp
$(BUILD_DIR)/Chessboard.o: src/Chessboard.cpp src/Chessboard.hpp src/coordinates.hpp src/Square.hpp src/Figures.hpp
$(BUILD_DIR)/ChessAI.o: src/ChessAI.cpp src/ChessAI.hpp src/coordinates.hpp src/Chessboard.hpp src/Square.hpp src/Figures.hpp
$(BUILD_DIR)/Game.o: src/Game.cpp src/Game.hpp src/Chessboard.hpp src/coordinates.hpp src/Square.hpp src/Figures.hpp src/ChessAI.hpp
$(BUILD_DIR)/Chess.o: src/Chess.cpp src/Chess.hpp src/Game.hpp src/Chessboard.hpp src/coordinates.hpp src/Square.hpp src/Figures.hpp src/ChessAI.hpp


